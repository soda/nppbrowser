<?php

if (!isset($_GET['mode'])) {
    header('Location: ./index.php?mode=NPP');
}

$mode = isset($_GET['mode']) ? $_GET['mode'] : 'NPP';

if (!isset($_GET['showAll']) && !isset($_GET['by'])) {
    require_once 'search.php';
    die();
}

require_once __DIR__ . '/../common.php';

$group = isset($_GET['name']) ? $_GET['name'] : '__all__';

$chosenGroup = $group;

if (isset($nppKeywords[$group]) || $group === '__uncategorized__') {
    $groups = [$group];
} else {
    $groups = array_keys($nppKeywords);
}

if ($group === '__all__') {
    $groups[] = '__uncategorized__';
}

$format = 'html';

if (isset($_GET['format']) && $_GET['format'] === 'wiki') {
    $format = 'wiki';
}

sort($groups);

$debug = isset($_GET['debug']);

// Bypass the cache if debug is set
if ( $debug ) {
    $str=rand();
    $randomizer = md5($str);
} else {
    $randomizer = '';
}

$cacheKey = md5(implode('|', $groups)) . '-' . $format . '-' . md5($mode) . $randomizer;

if ($record = $db->fetchOne('SELECT `content` FROM cache WHERE `key`=' . $db->quote($cacheKey))) {
    echo $record;
} else {
    ob_start();

    if ($format === 'html') {
        require __DIR__ . '/../includes/list.php';
    } else {
        require __DIR__ . '/../includes/list-wiki.php';
    }


    $content = ob_get_contents();
    if ( !$debug ) {
        $db->insert('cache', [
            'key' => $cacheKey,
            'content' => $content
        ]);
    }
}
