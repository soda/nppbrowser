<?php

require_once __DIR__ . '/../common.php';

$logs = $db->fetchAll('SELECT * FROM log ORDER BY id ASC');

$db->query('truncate table log_filtered');

$prev = $logs[0];

for ($i=1; $i<count($logs); $i++) {
    $log = $logs[$i];

    while (substr($log['search'], 0, strlen($prev['search'])) === $prev['search'] && $log['search'] !== $prev['search']) {
        $prev = $log;
        $i++;
    }

    if  ($log['search'] === $prev['search'] && $log['sort_field'] === $prev['sort_field'] && $log['sort_dir'] === $prev['sort_dir'] && $log['start'] === $prev['start'] && $log['length'] === $prev['length']) {
        continue;
    }

    $db->insert('log_filtered', $prev);

    if (isset($logs[$i+1])) {
        $prev = $logs[$i+1];
        $i++;
    }
}

//SELECT count(*), date(creation_date) as date FROM `log_filtered` GROUP BY date ORDER BY date ASC