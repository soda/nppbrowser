<?php
$mode = isset($_GET['mode']) ? $_GET['mode'] : 'NPP';
$oppositeMode = $mode === 'NPP' ? 'AFC' : 'NPP';
?>

<li>
    <?php if ($mode === 'NPP'): ?>
        <a href="./index.php?mode=<?=urlencode($oppositeMode)?>"><i class="fa fa-solid fa-folder"></i> Switch to AFC mode</a>
    <?php else: ?>
        <a href="./index.php?mode=<?=urlencode($oppositeMode)?>"><i class="fa fa-solid fa-book"></i> Switch to NPP mode</a>
    <?php endif; ?>
</li>

<li>
    <a href="./"><i class="fa fa-search fa-fw"></i> Search</a>
</li>

<li>
    <a href="./"><i class="fa fa-th-list fa-fw"></i> Browse<span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse in">
        <li>
            <a href="./index.php?showAll=1&mode=<?=urlencode($mode)?>">Show All</a>
        </li>
        <?php foreach ($nppKeywords as $group => $words): ?>
        <li>
            <a href="./index.php?by=group&mode=<?=urlencode( $mode )?>&name=<?= urlencode($group);?>"><?= htmlentities($group); ?></a>
        </li>
        <?php endforeach; ?>
        <li>
            <a href="./index.php?by=group&mode=<?=urlencode( $mode )?>&name=__uncategorized__">Uncategorized</a>
        </li>
        <!--<li>
            <a href="#">Second Level Item</a>
        </li>
        <li>
            <a href="#">Third Level <span class="fa arrow"></span></a>
            <ul class="nav nav-third-level collapse">
                <li>
                    <a href="#">Third Level Item</a>
                </li>
                <li>
                    <a href="#">Third Level Item</a>
                </li>
                <li>
                    <a href="#">Third Level Item</a>
                </li>
                <li>
                    <a href="#">Third Level Item</a>
                </li>
            </ul>
            <!-- /.nav-third-level -->
        </li>

    </ul>
    <!-- /.nav-second-level -->
</li>

<li>
    <p style="text-align:left;margin-left:16px;color:dimgrey;margin-top:16px">
        Updated: <?= $db->FetchOne('SELECT max(Creation_date) from article'); ?> UTC
    </p>
</li>

<li>
    <p style="text-align:left;margin-left:16px;color:dimgrey;margin-top:16px">
        Maintainers:
        <a href="https://en.wikipedia.org/wiki/User:Rentier" target="_blank">Rentier</a>
        (<a href="https://en.wikipedia.org/wiki/User_talk:Rentier" target="_blank">talk</a>), 
        <a href="https://en.wikipedia.org/wiki/User:MPGuy2824" target="_blank">MPGuy2824</a>
        (<a href="https://en.wikipedia.org/wiki/User_talk:MPGuy2824" target="_blank">talk</a>)
    </p>
</li>
