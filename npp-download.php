<?php

require_once __DIR__ . '/common.php';

ini_set('max_execution_time', 0);

function getIteratively(&$db, string $apiBaseUrl, int $acceptableTotalErrors, bool $isNpp = true) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = [];
        $errors = 0;
        $iteration = 0;
        $apiUrl = $apiBaseUrl;
        do {
                try {
                        curl_setopt($ch, CURLOPT_URL, $apiUrl);
                        $json = json_decode(curl_exec($ch), true);
                        $data = $json['pagetriagelist']['pages'];
                } catch(Exception $e) {
                        echo $e->getMessage();
                        $errors++;
                        if ( $errors >= $acceptableTotalErrors ) {
                                echo "Too many errors.\nQuitting at API call #$iteration.\n";
                                break;
                        } else {
                                echo "Error#$error at iteration #$iteration. Retrying...\n";
                                $data = [];
                        sleep(0.5); // courtesy rate limit
                                continue;
                        }
                }

                if (count($data) > 0) {
                        $apiUrl = $apiBaseUrl . '&pageoffset=' . $data[count($data) - 1]['pageid'] . '&offset=' . $data[count($data) - 1]['creation_date'];
                }

            foreach ($data as $page) {
                $db->insert('article_new', [
                    'wiki_id' => $page['pageid'],
                    'is_npp' => $isNpp,
                    'redirect' => (int)$page['is_redirect'],
                    'title' => substr($page['title'], 0, 255),
                    'category_count' => is_numeric($page['category_count']) ? $page['category_count'] : 999,
                    'linkcount' => is_numeric($page['linkcount']) ? $page['linkcount'] : 999,
                    'status' => (int)($page['csd_status'] || $page['prod_status'] || $page['blp_prod_status'] || $page['afd_status']),
                    'patrol_status' => $page['patrol_status'],
                    'revcount' => is_numeric($page['rev_count']) ? $page['rev_count'] : 999,
                    'afc_state' => is_numeric( $page['afc_state'] ) ? $page['afc_state'] : -1,
                    'length' => $page['page_len'],
                    'snippet' => $page['snippet'],
                    'creator_name' => substr($page['user_name'], 0, 255),
                    'creator_editcount' => is_numeric($page['user_editcount']) ? $page['user_editcount'] : 999,
                    'creator_autoconfirmed' => (int)$page['user_autoconfirmed'],
                    'creator_id' => $page['user_id'],
                    'creation_date' => date('Y-m-d H:i:s', strtotime($page['creation_date_utc']))
                ]);
            }

                $iteration++;
                if ($iteration % 10 == 0) {
                        echo $iteration.":". (microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]) ."\n";
                }
            //sleep(1); // courtesy rate limit
        } while (count($data) > 0);

        // close curl resource to free up system resources
        curl_close($ch);

        echo "Done.\nErrors: $errors\nAcceptable errors: $acceptableTotalErrors\nIterations: $iteration\n";
}


$baseUrl = 'https://en.wikipedia.org/w/api.php?action=pagetriagelist&format=json&showreviewed=1&showunreviewed=1&showredirs=1&showothers=1&limit=200&dir=newestfirst';
$url = $baseUrl;

$sql = file_get_contents(__DIR__ . '/sql/article.sql');
$arr = explode("-- Indexes for dumped tables", $sql);

$db->query($arr[0]);

$acceptableTotalErrors = 5;

$url .= '&namespace=0';

getIteratively($db, $url, $acceptableTotalErrors);

$url = $baseUrl . '&namespace=118';

getIteratively($db, $url, $acceptableTotalErrors, false);

$db->query($arr[1]); // create indexes and rename

$count = $db->fetchOne('SELECT count(*) FROM article');
$autoconfCount = $db->fetchOne('SELECT count(*) FROM article WHERE creator_autoconfirmed = 1');
$medianEditcount = $db->fetchOne('SELECT `creator_editcount` FROM article LIMIT ' . floor($count/2) . ',1');
$medianLength  = $db->fetchOne('SELECT `length` FROM article LIMIT ' . floor($count/2) . ',1');

$stat = [
    'count' => $count,
    'autoconfirmed_count' => $autoconfCount,
    'median_user_editcount' => $medianEditcount,
    'median_length' => $medianLength
];

$db->insert('stat', $stat);

print_r($stat);
$totalTime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
echo "Process Time: $totalTime seconds\n";
echo "Process Time: ".($totalTime/60)." minutes\n";

