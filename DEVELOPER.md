# Setup for Developers

## Preliminary

To setup nppbrowser locally, you will need the following prerequisite setup:
- [MariaDB](https://mariadb.org/)
- [PHP](https://www.php.net/) (Has been tested in 8.2, but other versions should work)

To setup your MariaDB database, run the following commands inside of the `mariadb -u root -p` interactive repl interface:
- `CREATE USER 'npp'@'localhost' IDENTIFIED BY 'npp';`
- `CREATE TABLE nppbrowser_test;`
- `GRANT ALL PRIVILEGES ON nppbrowser_test.* TO 'npp'@'localhost';`

This will create a dummy database called `nppbrowser_test` with a user `npp` (password: `npp`) who is able to perform arbitrary actions on the database.

To setup PHP, find the [php.ini](https://linuxconfig.net/manuals/howto/how-to-find-php-ini-file.html) file and make sure that the following line is not commented out:
```ini
extension=pdo_mysql
```

## Setting up the database

To setup the database, you will need to note down the absolute URL of the `./db/wiki.sql` file in the repo.

Run `mariadb -u npp -p` and drop into the interactive interface, after which you need to run the following commands:
- `use nppbrowser_test;`
- `source <full path to ./db/wiki.sql file>`

## Setting up the server

Copy the `config-dev.php` and replace the current `config.php` file. Make sure to not commit the changes to the `config.php` file when uploading a MR.

Once this is done, run `php npp-download.php` from the repo folder. This will download data about the current new pages feed from en.wikipedia.org and thus can take quite a while depending on the internet connection speed.

## Running the web app

To run the web app, run `php -S localhost:3000` in the repository folder. This should give you a test version of the app at http://localhost:3000/public/index.php.

## Debugging

Certain paths in the app use a database caching strategy to perform less intensive database queries while in production. This behaviour can be undesireable while debugging and can be bypassed by appending `&debug=true` to the URL.
