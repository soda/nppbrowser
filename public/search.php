<?php

require_once __DIR__ . '/../common.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NPP Browser</title>

    <!-- Bootstrap Core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="./vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="./vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">NPP Browser <small style="font-size:0.5em">v. 1.3.0</small></a>
        </div>
        <!-- /.navbar-header -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" maxlength="255" placeholder="Keyword or username" id="searchBox">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>

                    </li>
                    <?php require '../includes/menu.php'; ?>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <br/>
                Show:
                <br/>
                <?php if ($mode === 'NPP'): ?>
                <label style="font-weight:normal;margin-left:16px;"><input type="radio" checked="checked" name="type" value="pages"> articles</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" name="type" value="redirects"> redirects</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" name="type" value="both"> both</label>
                <br/>
                <label style="font-weight:normal;margin-left:16px;"><input type="radio" checked="checked" name="reviewed" value="unreviewed"> unreviewed</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" name="reviewed" value="allreviewed"> reviewed</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" name="reviewed" value="autopatrolled"> autopatrolled</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" name="reviewed" value="reviewednoautopatrolled"> reviewed without autopatrolled</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" name="reviewed" value="both"> all</label>
                <?php else: ?>
                <label style="font-weight:normal;margin-left:16px;"><input type="radio" name="afc_state" value="unsubmitted"> Unsubmitted</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" checked="checked" name="afc_state" value="awaiting_review"> Awaiting review</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" name="afc_state" value="under_review"> Under review</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" name="afc_state" value="declined"> Declined</label>
                <label style="font-weight:normal;margin-left:16px"><input type="radio" name="afc_state" value="all"> All</label>
                <?php endif; ?>
                <Br/><Br/>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-lg-12">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr>
                        <th style="min-width:5em;">Title</th>
                        <th style="min-width:20em;">Snippet</th>
                        <th>Del</th>
                        <th>Cats</th>
                        <th>Link</th>
                        <th>Revs</th>
                        <th>Length</th>
                        <th>Creator</th>
                        <th>Edits</th>
                        <th>Created</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="./vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="./vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="./vendor/raphael/raphael.min.js"></script>

<!-- DataTables JavaScript -->
<script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="./vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="./vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="./dist/js/sb-admin-2.js"></script>

<script src="./js/search.js?v=3"></script>

<?php if(isset($_GET['q'])): ?>
<script type="text/javascript">
    $(function(){
        $('#searchBox').val(<?= json_encode($_GET['q']); ?>).trigger('keyup');
    });
</script>

<?php endif;?>

</body>

</html>

