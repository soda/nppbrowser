<?php

function getArticlesByGroups($groups, $mode) {
    global $db;
    global $nppKeywords;

    $result = array();

    foreach ($groups as $g) {
        if ($g === '__uncategorized__') {
            $kw = array();

            foreach ($nppKeywords as $keywords) {
                $kw = array_merge($kw, $keywords);
            }

            $select = $db->select();
            $select->from('article', array('title', 'status','creator_editcount', 'linkcount', 'redirect'))
                   ->order('title ASC');

            foreach ($kw as $k) {
                if ( $mode === 'NPP' ) {
                    $select->where('is_npp = true');
                    $select->where('patrol_status = 0');
                } else if ( $mode === 'AFC' ) {
                    $select->where('is_npp = false');
                    $select->where('afc_state = 2');
                } else {
                    $select->where('patrol_status = 0');
                }
                $select->where('not match(snippet) against (?)', $k);
                $select->where('title NOT LIKE ?', "%$k%");
            }

            $result[$g] = array('none' => $db->fetchAll($select));
        } else {
            $result[$g] = array();

            $keywords = $nppKeywords[$g];

            foreach ($keywords as $k) {
                $select = $db->select();

                $select->from('article', array('title', 'status','creator_editcount', 'linkcount', 'redirect'))
                    ->order('title ASC');

                if ( $mode === 'NPP' ) {
                    $select->where('is_npp = true');
                    $select->where('patrol_status = 0');
                } else if ( $mode === 'AFC' ) {
                    $select->where('is_npp = false');
                    $select->where('afc_state = 2');
                } else {
                    $select->where('patrol_status = 0');
                }

                $select->where('match(snippet) against (?) OR title LIKE ?', $k, "%$k%");

                $articles = $db->fetchAll($select);

                if (count($articles) > 0) {
                    $result[$g][$k] = $articles;
                }
            }
        }
    }

    return $result;
}
