$(document).ready(function() {
    function rawurlencode(str){
        return str.replace(/\%/g, '%25');
    }

    $('#dataTables-example').DataTable({
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": 25,
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        "ajax": {
            "url": "./npp-api.php",
            "dataType": "jsonp",
            "data": function ( d ) {
                var url = new URL(location.href);
                d.mode = url.searchParams.get( 'mode' );
                if ( d.mode === 'NPP' ) {
                    d.type = $('input[name="type"]:checked').val();
                    d.reviewed = $('input[name="reviewed"]:checked').val();
                } else {
                    d.afcstate = $('input[name="afc_state"]:checked').val();
                }
            }
        },
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return '<small></small><a target="_blank" href="https://en.wikipedia.org/wiki/' + rawurlencode(data) + '?redirect=no&showcurationtoolbar=1">' + data + '</a></small>';
                },
                width: 150,
                "targets": 0
            },
            {
                "render": function ( data, type, row ) {
                    return '<small>' + data + '</small>';
                },
                "targets": 1
            },
            {
                "className": "dt-center",
                "render": function ( data, type, row ) {
                    if (data == 1) {
                        return '<i class="fa fa-trash-o"></i>';
                    }

                     return '';
                },
                "targets": 2
            },
            {
                "render": function ( data, type, row ) {
                    if (data === '999') {
                        data = '>99';
                    }

                    return data;
                },
                "targets": 3
            },
            {
                "render": function ( data, type, row ) {
                    if (data === '999') {
                        data = '>50';
                    }

                    var url = 'https://en.wikipedia.org/wiki/Special:WhatLinksHere/' + row[0];
                    return '<a target="_blank" href="' + url + '">' + data + '</a>';
                },
                "targets": 4
            },
            {
                "render": function ( data, type, row ) {
                    if (data === '999') {
                        data = '>99';
                    }

                    var url = 'https://en.wikipedia.org/w/index.php?title=' + row[0] + '&action=history';
                    return '<a target="_blank" href="' + url + '">' + data + '</a>';
                },
                "targets": 5
            },
            {
                "render": function ( data, type, row ) {
                    return Math.round(data/1000*10)/10 + 'K';
                },
                "targets": 6
            },
            {
                "render": function ( data, type, row ) {
                    return '<small></small><a target="_blank" href="https://en.wikipedia.org/wiki/User:' + data + '">' + data + '</a></small>';
                },
                width: 150,
                "targets": 7
            },
            {
                "render": function ( data, type, row ) {
                    var url  = 'https://en.wikipedia.org/w/index.php?limit=50&title=Special:Contributions&contribs=user&target=' + row[7] +'&namespace=0&tagfilter=&newOnly=1&start=&end=';

                    if (data >= 1000) {
                        data = Math.round(data/1000) + 'K';
                        data = '<span class="badge">' + data + '</span>';
                    }

                    return '<a target="_blank" href="' + url + '">' + data + '</a>';
                },
                width: 150,
                "targets": 8
            }
        ]
    });

    var searchDelay = null;

    function doSearch()
    {
        var search = $('#searchBox').val();

        clearTimeout(searchDelay);

        var table = $('#dataTables-example').DataTable();

        searchDelay = setTimeout(function() {
            if (search != null) {
                table.search(search).draw();
            }
        }, 200);
    }

    $('#searchBox').on('keyup', function() {
        doSearch();
    });

    $('input[type="radio"]').change(function(){
        doSearch();
    });
});

