<?php

$dataSources = [
    'curationActions' => [
        'legend' => ['Month', 'Review', 'Add deletion tags', 'Add other tags' ],
        'average' => 'daily',
        'sql' => "
            SELECT 
              SUM(action='reviewed') as reviewed, 
              SUM(action='delete') as deleted, 
              SUM(action='tag') as tagged,  
              EXTRACT( YEAR_MONTH FROM `timestamp` ) as yearmonth 
            FROM `enwiki_log_curation` 
            WHERE namespace=0
            GROUP BY yearmonth 
            ORDER BY yearmonth asc  
        "
    ],
    'pagesReviewed' => [
        'legend' => ['Month', 'Pages'],
        'average' => 'daily',
        'sql' => "
            SELECT  
              count(distinct title) as pages, 
              EXTRACT( YEAR_MONTH FROM `timestamp` ) as yearmonth 
            FROM `enwiki_log_curation` 
            WHERE namespace=0
            GROUP BY yearmonth 
            ORDER BY yearmonth asc        
        "
    ],
    'activeReviewers' => function() {
        $result = [
            'legend' => ['Month', '> 0', '> 10', '> 30', '> 100 actions in a month'],
            'sql' => []
        ];

        foreach ([0, 10, 30, 100] as $limit) {
            $result['sql'][] = "
              SELECT 
                count(*) as reviewers, 
                yearmonth from (
                  SELECT userid, EXTRACT( YEAR_MONTH FROM `timestamp` ) as yearmonth  
                  FROM `enwiki_log_curation` 
                  WHERE namespace=0                  
                  GROUP BY yearmonth,userid 
                  HAVING count(*)>$limit
               ) a
               GROUP BY a.yearmonth 
               ORDER BY a.yearmonth ASC
           ";
        }

        return $result;
    },
    'NPPBacklog' => [
        'legend' => ['Timestamp', 'Total pages', 'By autoconfirmed'],
        'sql' => "
            SELECT
                  `count` as total,
                  autoconfirmed_count as autoconfirmed,                 
                  creation_date as ts
                FROM                 
                  stat
        "
    ],
    'NPPBacklogNAutoconfirmedFraction' => [
        'legend' => ['Timestamp', '% Non-autoconfirmed'],
        'sql' => "
            SELECT
                  (1 - autoconfirmed_count/`count`)*100 as percentage,
                  creation_date as ts
                FROM                 
                  stat            
        "
    ],
    'NPPBacklogMedianEdits' => [
        'legend' => ['Timestamp', 'Median edit count'],
        'sql' => "
            SELECT
                  median_user_editcount,
                  creation_date as ts
                FROM                 
                  stat            
        "
    ],
    'Deletion' => function() {
        $strings = [
            'csd' => [
                'WP:CSD', 'speedy deletion criteria', 'G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7',' G8', 'G9', 'G10', 'G11', 'G12',
                'G 1', 'G 2', 'G 3', 'G 4', 'G 5', 'G 6', 'G 7',' G 8', 'G 9', 'G 10', 'G 11', 'G 12',
                'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10', 'A11', 'speedy deletion', 'speedy delete'
            ],
            'prod' => [
                'WP:PROD', 'expired PROD', 'expired prod', 'expired Prod', 'Expired PROD'
            ],
            'blpprod' => [
                'BLP prod', 'blp prod', 'blp PROD', 'BLPROD', 'BLPPROD'
            ],
            'afd' => [
                'Wikipedia:Articles for deletion', 'AfD'
            ],
            'exclude' => [
                'Neelix',
                'neelix',
                'Mass deletion',
                'redirect',
                'Redirect',
                'RfD',
                'WP:RFD'
            ]
        ];


        $queries = [];
        $subqueries = [];

        foreach ($strings as $key => $phrases) {
            $subqueries[$key] = [];

            foreach ($phrases as $p) {
                if ($key === 'exclude') {
                    $subqueries[$key][] = "locate('$p', comment) = 0";
                } else {
                    $subqueries[$key][] = "locate('$p', comment) > 0";
                }
            }

            if ($key === 'exclude') {
                continue;
            }

            $queries[] = 'sum(' . implode(' OR ', $subqueries[$key]) . ') as ' . $key;
        }

        $result = [
            'legend' => ['Month', 'CSD', 'PROD', 'BLPPROD', 'AfD'],
            'average' => 'daily',
            'sql' => "
            SELECT 
              " . implode(",\n", $queries) . ",
              EXTRACT( YEAR_MONTH FROM `timestamp` ) as yearmonth 
            FROM `enwiki_log_deletion`
             WHERE 
                namespace = 0 AND
                `action`='delete' AND
                " . implode(" AND ", $subqueries['exclude']) . "
             GROUP BY yearmonth 
             ORDER BY yearmonth asc
        "
        ];

        return $result;
    },

    'DeletionCSDBreakdown' => function() {
        $legend = ['Month'];

        $strings = [
            'exclude' => [
                'Neelix',
                'neelix',
                'Mass deletion',
                'redirect',
                'Redirect',
                'RfD',
                'AfD',
                'WP:RFD',
                'PROD',
                'BLPPROD'
            ]
        ];

        for ($i=1; $i<=13; $i++) {
            if ($i === 9) {
                continue;
            }

            $strings['G' . (string)$i] = [
                'G' . (string)$i, 'G ' . (string)$i
            ];

            $legend[] = 'G' . $i;
        }

        for ($i=1; $i<=11; $i++) {
            if ($i === 6 || $i=== 2 || $i===5 || $i===8 || $i===9) {
                continue;
            }

            $strings['A' . (string)$i] = [
                'A' . (string)$i, 'A ' . (string)$i
            ];

            $legend[] = 'A' . $i;
        }

        $queries = [];
        $subqueries = [];

        foreach ($strings as $key => $phrases) {
            $subqueries[$key] = [];

            foreach ($phrases as $p) {
                if ($key === 'exclude') {
                    $subqueries[$key][] = "locate('$p', comment) = 0";
                } else {
                    $subqueries[$key][] = "locate('$p', comment) > 0";
                }
            }

            if ($key === 'exclude') {
                continue;
            }

            if ($key === 'G1' || $key === 'A1') {
                $queries[] = 'sum(locate("10", comment)=0 AND locate("11", comment)=0 AND locate("12", comment)=0 AND locate("13", comment)=0 AND locate("14", comment)=0 AND (' . implode(' OR ', $subqueries[$key]) . ')) as ' . $key;
            } else {
                $queries[] = 'sum(' . implode(' OR ', $subqueries[$key]) . ') as ' . $key;
            }
        }

        $result = [
            'legend' => $legend,
            'average' => 'daily',
            'sql' => "
                SELECT 
                  " . implode(",\n", $queries) . ",
                  EXTRACT( YEAR_MONTH FROM `timestamp` ) as yearmonth 
                FROM `enwiki_log_deletion`
                 WHERE 
                    namespace = 0 AND
                    `action`='delete' AND
                    " . implode(" AND ", $subqueries['exclude']) . "
                 GROUP BY yearmonth 
                 ORDER BY yearmonth asc
            "
        ];

        return $result;
    }
];