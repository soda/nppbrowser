<?php

ini_set('display_errors', true);
ini_set('html_errors', false);
ini_set('user_agent', 'NPP-Browser');

error_reporting( E_ALL ^ ( E_NOTICE | E_DEPRECATED ) );

setlocale(LC_ALL, 'en_US.UTF-8');
setlocale(LC_TIME, 'UTC' );
date_default_timezone_set( 'UTC' );

set_include_path(realpath(__DIR__));

require_once __DIR__ . '/Zend/Db.php';
require_once __DIR__ . '/config.php';

$db = Zend_Db::factory('PDO_MYSQL', array(
    'host' => $dbHost,
    'username' => $dbUser,
    'password' => $dbPass,
    'dbname' => $dbName
));

$db->query("SET names utf8; SET CHARACTER SET 'UTF8'; SET character_set_database = 'UTF8'; SET character_set_connection = 'UTF8'; SET character_set_server = 'UTF8';");

require __DIR__ . '/npp-config.php';
require __DIR__ . '/npp-functions.php';