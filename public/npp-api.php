<?php

require_once __DIR__ . '/../common.php';

$table = 'article';

$primaryKey = 'id';

$columns = array(
    array( 'db' => 'title', 'dt' => 0 ),
    array( 'db' => 'snippet',  'dt' => 1 ),
    array( 'db' => 'status',  'dt' => 2 ),
    array( 'db' => 'category_count',  'dt' => 3 ),
    array( 'db' => 'linkcount',  'dt' => 4 ),
    array( 'db' => 'revcount',  'dt' => 5 ),
    array( 'db' => 'length',  'dt' => 6 ),
    array( 'db' => 'creator_name',  'dt' => 7),
    array( 'db' => 'creator_editcount',  'dt' => 8 ),
    array( 'db' => 'creation_date',  'dt' => 9 ),
    array( 'db' => 'creator_autoconfirmed',  'dt' => 10 )
);

$sql_details = array(
    'host' => $dbHost,
    'user' => $dbUser,
    'pass' => $dbPass,
    'db'   => $dbName
);

$db->insert('log', [
    'search' => $_GET['search']['value'],
    'sort_field' => $_GET['order'][0]['column'],
    'sort_dir' => $_GET['order'][0]['dir'],
    'start' => $_GET['start'],
    'length' => $_GET['length']
]);

require( __DIR__ . '/../vendor/ssp.class.php' );

$jsonp = preg_match('/^[$A-Z_][0-9A-Z_$]*$/i', $_GET['callback']) ?  $_GET['callback'] : false;

if ($jsonp) {
    $data = SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns);

    foreach ($data['data'] as &$r) {
        $r[0] = htmlentities($r[0]);
        $r[1] = htmlentities($r[1]);
        $r[7] = htmlentities($r[7]);

        $year = date('Y', strtotime($r[9]));
        $r[9] = date('M jS', strtotime($r[9]));

        if ($year !== date('Y')) {
            $r[9] .= '  (' . $year . ')';
        }

    } unset($r);

    header('Content-type: text/json');
    echo $jsonp . '(' . json_encode($data) . ');';
}


